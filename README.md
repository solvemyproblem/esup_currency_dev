# INSTALL #

# in webroot
composer require geoip2/geoip2:~2.0


# disable cache 
cd ~
mkdir modules
cd modules
git clone https://solvemyproblem@bitbucket.org/solvemyproblem/esup_currency_dev.git
cd ~/htdocs/VIRTUALHOST
modman link ~/modules/esup_currency_dev


### What is this repository for? ###

Sets Magento store currency based on GeoIP

