<?php
require_once Mage::getBaseDir('base') . '/vendor/autoload.php';

use GeoIp2\Database\Reader;

class Esup_Currency_Model_Store extends Mage_Core_Model_Store
{
    public function getDefaultCurrencyCode()
    {
        $debug = 1;

        $result = $this->getConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_DEFAULT);

        if (!isset($_SERVER['REMOTE_ADDR'])) {
            if ($debug == 1) {
                Mage::log("NO REMOTE_ADDR:" . $_SERVER['REMOTE_ADDR'],null,'geoip.log',true);
            }
            return $result;
        }
        if ($debug == 1) {
            Mage::log("REMOTE_ADDR:" . $_SERVER['REMOTE_ADDR'],null,'geoip.log',true);
        }

        $session_currency = Mage::getSingleton('core/session')->getGeoip_Processed();
        Mage::log("Currency Session value:" . $session_currency,null,'geoip.log',true);
        if ($session_currency) {
            if ($debug == 1) {
                Mage::log("Session set, returning:" . $_SERVER['REMOTE_ADDR'] . " " . $session_currency,null,'geoip.log',true);
            }
            return $session_currency;
        }

        // Load database
        $reader = new Reader(Mage::getBaseDir('base') . '/geoip/GeoLite2-Country_20171205/GeoLite2-Country.mmdb');
        // This creates the Reader object, which should be reused across
        // lookups.
        $record = $reader->country($_SERVER['REMOTE_ADDR']);
        /*
            $geoCountryCode = $record->country->isoCode ;
            $geoContinentCode = $record->continent-code ;
        */

        // spiders should see the store default currency
        if (isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/bot|crawl|google|slurp|spider|mediapartners/i',$_SERVER['HTTP_USER_AGENT'])) {
            if ($debug == 1) {
                Mage::log("BOT:" . $_SERVER['HTTP_USER_AGENT'],null,'geoip.log',true);
            }
            return $result;
        }

        
        $geoCountryCode = null;
        try {
            $geoCountryCode = $record->country->isoCode;

            if ($debug == 1) {
                Mage::log("geoCountryCode:" . $geoCountryCode,null,'geoip.log',true);
            }

        } catch (Exception $e) {
            // prevent NOTICE error - for example we are running the code on a localhost
        }

        switch ($geoCountryCode) {
            case "GB":
                $result = "GBP";
                break;
            case "DK":
                $result = "DKK";
                break;
            default:
                $geoContinentCode = null;
                // now grab the continent code and set further by general regions
                try {
                    $geoContinentCode = $record->continent->code;

                    if ($debug == 1) {
                        Mage::log("geoContinentCode:" . $geoContinentCode,null,'geoip.log',true);
                    }

                } catch (Exception $e) {
                    Mage::log( "EXCEPTION CAUGHT".$e->getMessage(), null, 'geoip.log',true);
                }
                
                // Now decide what currency to set depending on broad regions
                switch ($geoContinentCode) {
                    case "EU": // we'll set EUR for European countries
                        $result = "EUR";
                        break;
                    default:    // everything else uses the default store currency as set in config
                }
        }
        if ($debug == 1) {
            Mage::log("Result:" . $result,null,'geoip.log',true);
        }
        Mage::getSingleton('core/session')->setGeoip_Processed($result);
        return $result;

    }
}